package ru.softaria.legostaeva.testovoe.parser.data;

import com.google.common.collect.Maps;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import ru.softaria.legostaeva.testovoe.exception.NotCorrectDataException;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertTrue;

@DisplayName("Тесты на корректное получение данных из Reader'a, в котором находится JSON")
class DataParserTest {
    private static final DataParser DATA_PARSER = new DataParser();

    @Test
    @DisplayName("Проверка корректного получения данных")
    void parseMapFromJsonReader() throws IOException, NotCorrectDataException {
        InputStream inputStream = ClassLoader.getSystemResourceAsStream("example.json");
        Map<String, String> expected = new HashMap<>();
        expected.put("example", "example");
        expected.put("JSON", "json");
        try (InputStreamReader fileReader = new InputStreamReader(inputStream)) {
            assertTrue(Maps.difference(expected, DATA_PARSER.parseMapFromJsonReader(fileReader)).areEqual());
        }
    }
}