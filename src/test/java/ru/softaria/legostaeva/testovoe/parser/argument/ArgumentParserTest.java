package ru.softaria.legostaeva.testovoe.parser.argument;

import org.apache.commons.cli.ParseException;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import ru.softaria.legostaeva.testovoe.exception.NotCorrectDataException;

@DisplayName("Тесты на корректный разбор командной строки")
class ArgumentParserTest {
    private static final ArgumentParser ARGUMENT_PARSER = new ArgumentParser();

    @Test
    @DisplayName("Проверка разбора корректно поданых аргументов")
    void parseArguments_returnArguments_ifArgumentsAreCorrect() throws ParseException, NotCorrectDataException {
        String[] arguments = {"-y", "first", "-t", "second"};
        ArgumentsParsingResult expectedResult = new ArgumentsParsingResult();
        expectedResult.setYesterdayWebPagesFileName("first");
        expectedResult.setTodayWebPagesFileName("second");
        Assertions.assertThat(expectedResult)
                .isEqualToComparingFieldByFieldRecursively(ARGUMENT_PARSER.parseArguments(arguments));
    }

    @Test
    @DisplayName("Проверка разбора аргументов, один из которых null")
    void parseArguments_throwNoCorrectDataException_ifOneArgumentIsNull() {
        String[] arguments = {"-y", "first"};
        Assertions.assertThatThrownBy(() -> ARGUMENT_PARSER.parseArguments(arguments))
                .isInstanceOf(NotCorrectDataException.class)
                .hasMessage("Отсутствует один или более из необходимых аргументов.");
    }

    @Test
    @DisplayName("Проверка разбора пустого набора аргументов")
    void parseArguments_throwNoCorrectDataException_ifArgumentsAreEmpty() {
        String[] emptyArguments = {};
        Assertions.assertThatThrownBy(() -> ARGUMENT_PARSER.parseArguments(emptyArguments))
                .isInstanceOf(NotCorrectDataException.class)
                .hasMessage("Отсутствует один или более из необходимых аргументов.");
    }

    @Test
    @DisplayName("Проверка разбора аргументов с несуществующим ключом")
    void parseArguments_throwParseException_ifThereIsFakeKey(){
        String[] argumentsWithFakeKey = {"-y", "first", "-t", "second", "-fake"};
        Assertions.assertThatThrownBy(() -> ARGUMENT_PARSER.parseArguments(argumentsWithFakeKey))
                .isInstanceOf(ParseException.class);
    }
}