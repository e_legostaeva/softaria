package ru.softaria.legostaeva.testovoe.message;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import ru.softaria.legostaeva.testovoe.exception.NotCorrectDataException;
import ru.softaria.legostaeva.testovoe.parser.data.difference.DifferenceParser;
import ru.softaria.legostaeva.testovoe.parser.data.difference.DifferenceResult;

import java.util.HashMap;
import java.util.Map;

@DisplayName("Тесты на корректное составление сообщения")
class MessageCreatorTest {
    private static final MessageCreator MESSAGE_CREATOR = new MessageCreator();
    private static final DifferenceParser DIFFERENCE_PARSER = new DifferenceParser();

    @Test
    @DisplayName("Проверка корректности сообщения при заполненном DifferenceResult")
    void createMessage_returnMessage_ifDifferenceResultCorrect() throws NotCorrectDataException {
        Map<String, String> first = new HashMap<>();
        Map<String, String> second = new HashMap<>();
        first.put("diff", "diff");
        second.put("diff", "different");
        first.put("deleted", "deleted");
        second.put("new", "new");

        DifferenceResult differenceResult = DIFFERENCE_PARSER.getDifference(first, second);
        System.out.println(MESSAGE_CREATOR.createMessage(differenceResult));
        Assertions.assertEquals("Здравствуйте, дорогая Валерия Валерьевна!" + System.lineSeparator() +
                "За последние сутки во вверенных Вам сайтах произошли следующие изменения:" + System.lineSeparator() +
                "Исчезли следующие страницы: [deleted]" + System.lineSeparator() +
                "Появились следующие новые страницы: [new]" + System.lineSeparator() +
                "Изменились следующие страницы: [diff]" + System.lineSeparator() +
                System.lineSeparator() +
                "С уважением," + System.lineSeparator() +
                "автоматизированная система" + System.lineSeparator() +
                "мониторинга.", MESSAGE_CREATOR.createMessage(differenceResult));
    }

    @Test
    @DisplayName("Проверка корректной обрабоки при передаче DifferenceResult = null")
    void createMessage_throwNotCorrectDataException_ifDifferenceResultIsNull() {
        org.assertj.core.api.Assertions.assertThatThrownBy(() -> MESSAGE_CREATOR.createMessage(null))
                .isInstanceOf(NotCorrectDataException.class)
                .hasMessage("Не удалось создать сообщение от differenceResult = null");
    }
}