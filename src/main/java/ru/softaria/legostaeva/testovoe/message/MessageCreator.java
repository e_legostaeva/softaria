package ru.softaria.legostaeva.testovoe.message;

import ru.softaria.legostaeva.testovoe.exception.NotCorrectDataException;
import ru.softaria.legostaeva.testovoe.parser.data.difference.DifferenceResult;

public class MessageCreator {
    /**
     * Генерирует сообщение, которое необходимо отправить.
     *
     * @param differenceResult экземпляр класса {@link MessageCreator}
     * @return сообщение.
     */
    public String createMessage(DifferenceResult differenceResult) throws NotCorrectDataException {
        if (differenceResult == null) {
            throw new NotCorrectDataException("Не удалось создать сообщение от differenceResult = null");
        }

        return new StringBuilder().append("Здравствуйте, дорогая Валерия Валерьевна!")
                .append(System.lineSeparator())
                .append("За последние сутки во вверенных Вам сайтах произошли следующие изменения:")
                .append(System.lineSeparator())
                .append("Исчезли следующие страницы: ")
                .append(differenceResult.getDeletedWebPages().keySet())
                .append(System.lineSeparator())
                .append("Появились следующие новые страницы: ")
                .append(differenceResult.getNewWebPages().keySet())
                .append(System.lineSeparator())
                .append("Изменились следующие страницы: ")
                .append(differenceResult.getDifference().keySet())
                .append(System.lineSeparator()).append(System.lineSeparator())
                .append("С уважением,")
                .append(System.lineSeparator())
                .append("автоматизированная система")
                .append(System.lineSeparator())
                .append("мониторинга.")
                .toString();
    }
}
