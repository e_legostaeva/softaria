package ru.softaria.legostaeva.testovoe.message.sender;

import ru.softaria.legostaeva.testovoe.Testovoe;
import ru.softaria.legostaeva.testovoe.exception.NotCorrectDataException;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.util.Properties;

public class MessageSender {
    private static final String MIME_MESSAGE_SUBJECT = "Изменения в сайтах";
    private static final String MAIL_PROPERTIES = "mail.properties";
    private static final String SENDER_EMAIL = "mail.smtps.user";
    private static final String RECIPIENT = "recipient.email";
    private static final String SENDER_PASSWORD = "sender.password";
    private final Properties properties = new Properties();

    /**
     * Отправляет email-сообщение. Необходимые настройки, такие как почта отправителя и получателя, пароль отправителя,
     * берутся из mail.properties
     *
     * @param message сообщение, которое необходимо отправить.
     * @throws NotCorrectDataException если произошли проблемы во время:
     *                                 <li>Генерации сообщения для отправки
     *                                 <li>Попытки открыть соединение.
     */
    public void sendMessage(String message) throws NotCorrectDataException {
        try {
            properties.load(Testovoe.class.getClassLoader().getResourceAsStream(MAIL_PROPERTIES));
        } catch (IOException e) {
            throw new NotCorrectDataException("Произошла ошибка во время обработки " + MAIL_PROPERTIES);
        }

        Session mailSession = Session.getDefaultInstance(properties);
        MimeMessage mimeMessage = new MimeMessage(mailSession);
        try {
            mimeMessage.setFrom(new InternetAddress(properties.getProperty(SENDER_EMAIL)));
            mimeMessage.addRecipients(Message.RecipientType.TO, new InternetAddress[]{
                    new InternetAddress(properties.getProperty(RECIPIENT)
                    )});
            mimeMessage.setSubject(MIME_MESSAGE_SUBJECT);
            mimeMessage.setText(message);
        } catch (MessagingException e) {
            throw new NotCorrectDataException("Ошибка во время настройки сообщения для отправки.");
        }

        try (Transport transport = mailSession.getTransport()) {
            transport.connect(null, properties.getProperty(SENDER_PASSWORD));
            transport.sendMessage(mimeMessage, mimeMessage.getAllRecipients());
        } catch (MessagingException e) {
            throw new NotCorrectDataException(e.getMessage());
        }
    }
}
