package ru.softaria.legostaeva.testovoe;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.cli.ParseException;
import ru.softaria.legostaeva.testovoe.exception.NotCorrectDataException;
import ru.softaria.legostaeva.testovoe.file.FileProvider;
import ru.softaria.legostaeva.testovoe.message.MessageCreator;
import ru.softaria.legostaeva.testovoe.message.sender.MessageSender;
import ru.softaria.legostaeva.testovoe.parser.argument.ArgumentParser;
import ru.softaria.legostaeva.testovoe.parser.argument.ArgumentsParsingResult;
import ru.softaria.legostaeva.testovoe.parser.data.DataParser;
import ru.softaria.legostaeva.testovoe.parser.data.DataParsingResult;
import ru.softaria.legostaeva.testovoe.parser.data.difference.DifferenceParser;
import ru.softaria.legostaeva.testovoe.parser.data.difference.DifferenceResult;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;

@Slf4j
public class Testovoe {
    private static final ArgumentParser ARGUMENT_PARSER = new ArgumentParser();
    private static final FileProvider FILE_PROVIDER = new FileProvider();
    private static final DataParser DATA_PARSER = new DataParser();
    private static final DifferenceParser DIFFERENCE_PARSER = new DifferenceParser();
    private static final MessageCreator MESSAGE_CREATOR = new MessageCreator();
    private static final MessageSender MESSAGE_SENDER = new MessageSender();

    public static void main(String[] args) {
        File yesterdayFile;
        File todayFile;
        ArgumentsParsingResult argumentsParsingResult;
        try {
            argumentsParsingResult = ARGUMENT_PARSER.parseArguments(args);
        } catch (ParseException | NotCorrectDataException e) {
            log.error("Возникли проблемы при обработке командной строки.", e);
            return;
        }

        try {
            yesterdayFile = FILE_PROVIDER.getFile(argumentsParsingResult.getYesterdayWebPagesFileName());
            todayFile = FILE_PROVIDER.getFile(argumentsParsingResult.getTodayWebPagesFileName());
        } catch (NotCorrectDataException e) {
            log.error("Входные файлы не существуют, либо нет прав на чтение.", e);
            return;
        }

        DataParsingResult dataParsingResult = new DataParsingResult();
        try (
                FileReader yesterdayFileReader = new FileReader(yesterdayFile);
                FileReader todayFileReader = new FileReader(todayFile)
        ) {
            dataParsingResult.setYesterdayWebPages(DATA_PARSER.parseMapFromJsonReader(yesterdayFileReader));
            dataParsingResult.setTodayWebPages(DATA_PARSER.parseMapFromJsonReader(todayFileReader));
        } catch (NotCorrectDataException e) {
            log.error(e.getMessage());
            return;
        } catch (FileNotFoundException e) {
            log.error("Cannot find input file.");
            return;
        } catch (IOException e) {
            log.error("Cannot open or read input file.");
            return;
        }

        Map<String, String> yesterdayWebPagesMap = dataParsingResult.getYesterdayWebPages();
        Map<String, String> todayWebPagesMap = dataParsingResult.getTodayWebPages();

        DifferenceResult differenceResult = DIFFERENCE_PARSER.getDifference(yesterdayWebPagesMap, todayWebPagesMap);

        try {
            MESSAGE_SENDER.sendMessage(MESSAGE_CREATOR.createMessage(differenceResult));
        } catch (NotCorrectDataException e) {
            log.error("Ошибка во время создания или отправки сообщения", e);
        }
    }
}
