package ru.softaria.legostaeva.testovoe.parser.data.difference;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.Map;

@Getter
@Setter
@Accessors(chain = true)
public class DifferenceResult {
    private Map<String, String> deletedWebPages;
    private Map<String, String> newWebPages;
    private Map<String, String> difference;
}
