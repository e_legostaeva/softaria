package ru.softaria.legostaeva.testovoe.parser.argument;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class ArgumentsParsingResult {
    private String yesterdayWebPagesFileName;

    private String todayWebPagesFileName;
}
