package ru.softaria.legostaeva.testovoe.parser.data.difference;

import com.google.common.collect.MapDifference;
import com.google.common.collect.Maps;
import ru.softaria.legostaeva.testovoe.message.MessageCreator;

import java.util.Map;

public class DifferenceParser {
    /**
     * Генерирует экземпляр класса {@link MessageCreator}, содержащий разницу между параметрами.
     *
     * @return {@link MessageCreator}
     */
    @SuppressWarnings("unchecked")
    public DifferenceResult getDifference(Map<String, String> first, Map<String, String> second) {
        MapDifference mapDifference = Maps.difference(first, second);
        return new DifferenceResult()
                .setDeletedWebPages(mapDifference.entriesOnlyOnLeft())
                .setNewWebPages(mapDifference.entriesOnlyOnRight())
                .setDifference(mapDifference.entriesDiffering());
    }
}
