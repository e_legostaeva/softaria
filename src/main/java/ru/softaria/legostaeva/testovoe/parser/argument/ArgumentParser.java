package ru.softaria.legostaeva.testovoe.parser.argument;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.cli.*;
import ru.softaria.legostaeva.testovoe.exception.NotCorrectDataException;

import javax.annotation.Nonnull;

@Slf4j
public class ArgumentParser {
    private static final String YESTERDAY_FILE_OPTION_NAME = "y";
    private static final String TODAY_FILE_OPTION_NAME = "t";
    private static final String YESTERDAY_FILE_LONG_OPTION_NAME = "yesterday";
    private static final String TODAY_FILE_LONG_OPTION_NAME = "today";
    private static final CommandLineParser COMMAND_LINE_PARSER = new DefaultParser();
    private static final Options ARGUMENTS_OPTIONS = new Options()
            .addOption(Option.builder(YESTERDAY_FILE_OPTION_NAME)
                    .required(false)
                    .longOpt(YESTERDAY_FILE_LONG_OPTION_NAME)
                    .hasArg(true)
                    .build()
            )
            .addOption(Option.builder(TODAY_FILE_OPTION_NAME)
                    .required(false)
                    .longOpt(TODAY_FILE_LONG_OPTION_NAME)
                    .hasArg(true)
                    .build());

    /**
     * Обрабатывает командную строку, извлекая имена файлов со вчерашними данными и с данными, пришедшими сегодня.
     *
     * @param args аргументы программы.
     * @return {@link ArgumentsParsingResult}, который содержит имена
     * вчерашнего и сегодняшнего файлов, если они существуют.
     * @throws ParseException если возникли проблемы при обработке командной строки.
     */
    @Nonnull
    public ArgumentsParsingResult parseArguments(String[] args) throws ParseException, NotCorrectDataException {
        ArgumentsParsingResult argumentsParsingResult = new ArgumentsParsingResult();
        CommandLine commandLine = COMMAND_LINE_PARSER.parse(ARGUMENTS_OPTIONS, args);

        argumentsParsingResult.setYesterdayWebPagesFileName(commandLine.getOptionValue(YESTERDAY_FILE_OPTION_NAME));
        argumentsParsingResult.setTodayWebPagesFileName(commandLine.getOptionValue(TODAY_FILE_OPTION_NAME));

        if (argumentsParsingResult.getYesterdayWebPagesFileName() == null
                || argumentsParsingResult.getTodayWebPagesFileName() == null) {
            throw new NotCorrectDataException("Отсутствует один или более из необходимых аргументов.");
        }

        log.debug("Результат: {}", argumentsParsingResult);
        return argumentsParsingResult;
    }
}
