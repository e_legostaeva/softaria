package ru.softaria.legostaeva.testovoe.parser.data;

import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import ru.softaria.legostaeva.testovoe.exception.NotCorrectDataException;

import javax.annotation.WillNotClose;
import java.io.IOException;
import java.io.Reader;
import java.util.Map;

public class DataParser {
    private static final JSONParser JSON_PARSER = new JSONParser();

    /**
     * Из экземдяра класса {@link Reader} извлекает {@code JSON} и создает {@code Map<String, String>} с его помощью.
     */
    @SuppressWarnings("unchecked")
    public Map<String, String> parseMapFromJsonReader(@WillNotClose Reader reader) throws NotCorrectDataException {
        try {
            return (Map<String, String>) JSON_PARSER.parse(reader);
        } catch (IOException | ParseException e) {
            throw new NotCorrectDataException("Проблемы во время анализа JSON'а.");
        }
    }
}
