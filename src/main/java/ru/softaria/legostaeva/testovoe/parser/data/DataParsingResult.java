package ru.softaria.legostaeva.testovoe.parser.data;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.annotation.ParametersAreNonnullByDefault;
import java.util.HashMap;
import java.util.Map;

@Setter
@Getter
@ToString
@ParametersAreNonnullByDefault
public class DataParsingResult {
    private Map<String, String> yesterdayWebPages = new HashMap<>();
    private Map<String, String> todayWebPages = new HashMap<>();
}
