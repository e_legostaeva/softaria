package ru.softaria.legostaeva.testovoe.exception;

/**
 * Исключение, связанное с некорректными данными.
 */
public class NotCorrectDataException extends Exception {
    public NotCorrectDataException(String message) {
        super(message);
    }
}
