package ru.softaria.legostaeva.testovoe.file;

import lombok.extern.slf4j.Slf4j;
import ru.softaria.legostaeva.testovoe.exception.NotCorrectDataException;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.File;

@Slf4j
public class FileProvider {
    /**
     * Получает имя входного файла. Если такой файл существует, возвращает его.
     *
     * @param inputFileName имя входного файла.
     * @return входной файл.
     * @throws NotCorrectDataException если при работе с файлом по умолчанию произошла одна из следующих ошибок:
     *                                 <li>Нет прав на чтение файла;
     *                                 <li>Файл не существует.
     */
    @Nonnull
    public File getFile(@Nullable String inputFileName) throws NotCorrectDataException {
        if (inputFileName == null) {
            throw new NotCorrectDataException("Файл не найден.");
        }

        File inputFile = new File(inputFileName);
        if (!fileIsValid(inputFile)) {
            log.warn("Файл " + inputFile.getName() + " не существует, либо нет прав на чтение.");
            throw new NotCorrectDataException("Нет корректных данных в " + inputFile.getName());
        } else {
            return inputFile;
        }
    }

    private boolean fileIsValid(File file) {
        return (file.exists()) && (file.canRead());
    }
}
